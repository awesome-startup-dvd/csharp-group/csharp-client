﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend;

namespace Frontend.MVVM.Model
{
	class ContactModel
	{
		public string Username { get; set; }
		public string ImageSource { get; set; }

		public ObservableCollection<MessageModel> Messages { get; set; }
		public string LastMessage => Messages.Last().Message;

		public ContactModel(
			string username,
			string imageSource,
			ObservableCollection<MessageModel> messages)
		{
			Username = username;
			ImageSource = imageSource;
			Messages = messages;
		}

		public ContactModel(UserData userData)
		{
			Username = userData.login;
			ImageSource = userData.imageSource;
		}
	}
}
