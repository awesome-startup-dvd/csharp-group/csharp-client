﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Frontend.MVVM.Model
{
	class MessageModel
	{
		public string Login { get; set; }
		public string LoginColor { get; set; }
		public string ImageSource { get; set; }
		public string Message { get; set; }
		public DateTime Time { get; set; }
		public bool IsNativeOrigin { get; set; }
		public bool? FirstMessage { get; set; }
	}
}
