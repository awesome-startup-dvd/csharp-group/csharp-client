﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend;
using Frontend.Core;
using Frontend.MVVM.Model;

namespace Frontend.MVVM.ViewModel
{
	class MainViewModel : ObservableObject
	{
		public ObservableCollection<MessageModel> Messages { get; set; }
		public ObservableCollection<ContactModel> Contacts { get; set; }

		/*Commands*/

		public RelayCommand SendCommand { get; set; }

		private ContactModel _selectedContact;

		public ContactModel SelectedContact
		{
			get { return _selectedContact; }
			set 
			{
				_selectedContact = value;
				OnPropertyChanged();
			}
		}


		private string _message;

		public string Message
		{
			get { return _message; }
			set 
			{ 
				_message = value;
				OnPropertyChanged();
			}

		}

		public MainViewModel()
		{
			ConnectionHandler.Connection();
			UserData user = new UserData(UserDataRequest.getUserData("Admin", "Admin"));
			Messages = new ObservableCollection<MessageModel>();
			Contacts = new ObservableCollection<ContactModel>();

			SendCommand = new RelayCommand(o => 
			{
				Messages.Add(new MessageModel
				{
					Message = Message,
					FirstMessage = false
				});

				Message = "";

			});

			for (int i = 0; i < user.contacts.Count; i++)
			{
				for (int j = 0; j < user.contacts[i].contact.messages.Length; j++)
				{
					Messages.Add(new MessageModel
					{
						Login = user.contacts[i].contact.messages[j].message.login,
						LoginColor = user.contacts[i].contact.messages[j].message.login_color,
						ImageSource = "https://imgur.com/zHnmkmB.png",
						Message = user.contacts[i].contact.messages[j].message.message,
						Time = 
							DateTime.ParseExact(
							user.contacts[i].contact.messages[j].message.time, "HH:mm",
							System.Globalization.CultureInfo.InvariantCulture),
						IsNativeOrigin = false,
						FirstMessage = user.contacts[i].contact.messages[j].message.first_message
					}) ;
				}
			}

			for (int i = 0; i < user.contacts.Count; i++)
			{
				Contacts.Add(new ContactModel(
					user.contacts[i].contact.username,
					"https://imgur.com/zHnmkmB.png",
					Messages));
			}
		}
	}
}
