﻿using System;
using System.Collections.Generic;
using System.Text;
using UserApi;

namespace Backend
{
	public class UserData
	{
		public int id { get; private set; }
		public string imageSource { get; set; }
		public string login { get; private set; }
		public string password { get; private set; }
		public UserApi.User.Types.AccessLvl accessLevel { get; private set; }
		public PersonalData personData { get; private set; }
		public List<Contact> contacts = new List<Contact>();

		public UserData(
			string login,
			string password,
			UserApi.User.Types.AccessLvl accessLvl,
			PersonalData personalData,
			List<Contact> contacts)
		{
			this.login = login;
			this.password = password;
			accessLevel = accessLvl;
			personData = personalData;
			this.contacts = contacts;
		}

		public UserData(UserData userData)
		{
			id = userData.id;
			imageSource = userData.imageSource;
			login = userData.login;
			password = userData.password;
			accessLevel = userData.accessLevel;
			this.contacts = userData.contacts;
		}

		public UserData(GetUserDataResponce reply)
		{
			login = reply.User.Login;
			password = reply.User.Password;
			accessLevel = reply.User.AccessLvl;
			personData = new PersonalData(reply.User.PersonalData);
			contacts = Serelization.ContactToList(reply.User.ArrayContacts);
		}
	}
}