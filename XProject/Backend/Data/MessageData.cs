﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Backend
{
	public class MessageData
	{
		public string login { get; set; }
		public string login_color { get; set; }
		public string image_source { get; set; }
		public string message { get; set; }
		public string time { get; set; }
		public bool first_message { get; set; }

	}
	public class Message
	{
		public MessageData message { get; set; }
	}
}
