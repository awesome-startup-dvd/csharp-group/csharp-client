﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Backend
{

	public class ContactData
	{
		public string username { get; set; }
		public string image_source { get; set; }
		public string last_message { get; set; }

		public Message[] messages { get; set; }

		public ContactData(
			string _username,
			string imageSource,
			string lastMessage,
			Message[] _messages)
		{
			username = _username;
			image_source = imageSource;
			last_message = lastMessage;
			messages = _messages;
		}
	}
	public class Contact
	{
		public ContactData contact { get; set; }
	}

	public class RootContactData
	{
		public Contact[] contacts { get; set; }
	}
}
