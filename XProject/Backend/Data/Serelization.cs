﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace Backend
{
	public static class Serelization
	{
		public static List<Contact> ContactToList(string json)
		{
			return JsonConvert.DeserializeObject<RootContactData>(json).contacts.ToList<Contact>();
		}

	}
}
