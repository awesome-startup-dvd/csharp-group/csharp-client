﻿using System;
using System.Collections.Generic;
using System.Text;
using UserApi;

namespace Backend
{
	public static class UserDataRequest
	{
		public static UserData getUserData(string login, string password)
		{
			UserData userData;
			var client = new GetUser.GetUserClient(ConnectionHandler.Channel);
			var temp_request = new GetUserDataRequest
			{
				Login = login,
				Password = password
			};
			var reply = client.GetUserData(temp_request);

			userData = new UserData(reply);

			return userData;
		}
	}
}
